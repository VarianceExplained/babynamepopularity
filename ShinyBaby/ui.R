#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Shiny Babies!"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      uiOutput("yearUI"),
      
      sliderInput("nObs",label = "How many names to display:", min = 5,max = 25,value = 5),
      
      br(),
      
      br(),
      
      h2("Today's Suggested Names"),
      
      h3("Boy's Names:"),
      
      dataTableOutput("mNames"),
      
      actionButton("mRefresh", "Refresh List"),
      
      br(),
      
      h3("Girl's Names:"),
      
      dataTableOutput("fNames"),
      
      actionButton("fRefresh", "Refresh List")
      
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      
      fluidRow(
        column(6,plotOutput("pM")),

        column(6,plotOutput("pF"))),
      
      fluidRow(column(6,dataTableOutput("tM")),
       
        column(6,dataTableOutput("tF")))
      
    )
  )
))
